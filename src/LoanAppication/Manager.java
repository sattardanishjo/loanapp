package LoanAppication;

public class Manager {

	public boolean decision(LoanApplication fromanalyzer) {

		if (fromanalyzer.highRIsk) {

			System.out.println("Status is denied!");
			return fromanalyzer.isApproved;
		}

		else {
			System.out.println("Status is approved");
			fromanalyzer.isApproved = true;
			return fromanalyzer.isApproved;
		}

	}
}