package com.abdul;

import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) {

		ExceptionDemo obj = new ExceptionDemo();
		try {
			obj.cashOut();

		} catch (InsufficientFundException inv) {
			System.out.println(inv.customerMessage);

			inv.printStackTrace();
		}
	}

	public void cashOut() throws InsufficientFundException {

		Scanner customer = new Scanner(System.in);
		System.out.println("please enter your cashout pin");

		String pin = customer.nextLine();

		if (pin.equals("9000")) {
			System.out.println("Transfer complete");

		} else {

			InsufficientFundException I = new InsufficientFundException("please enter ur cashout pin");

			throw I;
		}
	}

}
