package com.abdul;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class QueryOperation {

	public static void main(String[] args) {
		Connection conn = null;

		try {
			conn = JdbcConection.getDBConnection();
			conn.setAutoCommit(false);
			insertTeacher("abdul", "danishjo", conn);

			conn.commit();
		} catch (Exception e) {
			e.printStackTrace();

			try {
				conn.close();

			} catch (SQLException e1) {
			}

		}

	}

	public static void insertTeacher(String first_name, String last_name, Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("insert into teacher (first_name,last_name) value(?,?)");
			stmt.setString(1, first_name);
			stmt.setString(2, last_name);
			int count = stmt.executeUpdate();
			System.out.println("Record Created" + "  " + count);

		} catch (SQLException e) {

			throw e;

		} finally {

			try {
				stmt.close();
			} catch (SQLException s) {
			}
		}

	}

}
