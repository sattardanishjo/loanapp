package com.abdul;

public class Calculator {

	public void arithmetic(int a, int b) {

		System.out.println("\n input provided for arithemtic are " + a + " and " + b);
		int sum = a + b;
		System.out.println("addition :" + sum);
		int sub = a - b;
		System.out.println("substraction :" + sub);
		int mul = a * b;
		System.out.println("multiplication :" + mul);
		int div = a / b;
		System.out.println("division :" + div);
		int mod = a % b;
		System.out.println("modulos" + mod);
	}

	public void unary(int x) {
		int singleMinuse = -x;
		System.out.println("single Minuse :" + singleMinuse);
		int singlePluse = +x;
		System.out.println("single pluse :" + singlePluse);
		System.out.println("post increment :" + (x++));
		System.out.println("pre increment :" + (++x));
		System.out.println("post decrement :" + (x--));
		System.out.println("pre decrement :" + (--x));
		boolean alive = true;
		System.out.println(!alive);

		String name = "michael";
		System.out.println(name instanceof String);

	}

	public void relational(int p, int q) {

		System.out.println("multiple condition satisfy check " + (p != q && p < q));

	}

	public void ternary(int a, int b) {

		String greaterResult = "";
		greaterResult = (a > b) ? "a is greater" : "b is greater";
		System.out.println("ternary operator result check :" + greaterResult);

	}

}
