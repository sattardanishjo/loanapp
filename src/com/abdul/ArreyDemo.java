package com.abdul;

import java.util.Scanner;

public class ArreyDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("=============== 2D Int Array Declaration=================");
		System.out.print("Enter the number of rows: ");
		int IntTowDRow = input.nextInt();
		System.out.print("Enter the number of columns: ");
		int IntTowDCol = input.nextInt();
		int TowDIntArray[][] = new int[IntTowDCol][IntTowDCol];
		for (int i = 0; i < TowDIntArray.length; i++) {
			for (int j = 0; j < TowDIntArray[i].length; j++) {
				System.out.print("Enter the value for [" + i + "][" + j + "]");
				TowDIntArray[i][j] = input.nextInt();

			}
		}
		System.out.println();
		System.out.println("=============== 2D String Array Declaration=================");
		System.out.print("Enter the number of rows: ");
		int StringTwoDRow = input.nextInt();
		System.out.print("Enter the number of columns: ");
		int StringTowDCol = input.nextInt();
		String TowDStringArray[][] = new String[StringTwoDRow][StringTowDCol];
		for (int i = 0; i < TowDStringArray.length; i++) {
			for (int j = 0; j < TowDStringArray[i].length; j++) {
				System.out.print("Enter a name for [" + i + "][" + j + "]");
				TowDStringArray[i][j] = input.next();
			}
		}

		System.out.println();
		System.out.println("=============== 3D Int Array Declaration=================");
		System.out.print("Enter the value for X : a[X][][] ");
		int x1 = input.nextInt();
		System.out.print("Enter the value for X : a[" + x1 + "][X][] ");
		int x2 = input.nextInt();
		System.out.print("Enter the value for X : a[" + x1 + "][" + x2 + "][X] ");
		int x3 = input.nextInt();
		System.out.println("\n=============Array Declaration Looks LIke ===================");
		System.out.println("\n        int[][][] a = new int[" + x1 + "][" + x2 + "][" + x3 + "];\n");
		System.out.println("============================================================\n");
		int[][][] a = new int[x1][x2][x3];

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				for (int k = 0; k < a[i][j].length; k++) {
					System.out.print("Enter the a number for [" + i + "][" + j + "]" + "][" + k + "]");
					a[i][j][k] = input.nextInt();
				}
			}
		}

		System.out.println();
		System.out.println("=============== 3D String Array Declaration=================");
		System.out.print("Enter the value for X : a[X][][] ");
		int s1 = input.nextInt();
		System.out.print("Enter the value for X : a[" + s1 + "][X][] ");
		int s2 = input.nextInt();
		System.out.print("Enter the value for X : a[" + s1 + "][" + s2 + "][X] ");
		int s3 = input.nextInt();
		System.out.println("\n=============Array Declaration Looks LIke ===================");
		System.out.println("\n        int[][][] s = new int[" + s1 + "][" + s2 + "][" + s3 + "];\n");
		System.out.println("============================================================\n");
		String[][][] s = new String[s1][s2][s3];

		for (int i = 0; i < s.length; i++) {
			for (int j = 0; j < s[i].length; j++) {
				for (int k = 0; k < s[i][j].length; k++) {
					System.out.print("Enter the a name for [" + i + "][" + j + "]" + "][" + k + "]");
					s[i][j][k] = input.next();
				}
			}
		}

		System.out
				.println("=================================2D Int Array Elements====================================");

		for (int i = 0; i < TowDIntArray.length; i++) {
			for (int j = 0; j < TowDIntArray[i].length; j++) {

				System.out.print(TowDIntArray[i][j] + " ");
			}
			System.out.println("");
		}

		System.out.println(
				"=================================2D String Array Elements====================================");

		for (int i = 0; i < TowDStringArray.length; i++) {
			for (int j = 0; j < TowDStringArray[i].length; j++) {

				System.out.print(TowDStringArray[i][j] + " ");
			}
			System.out.println("");
		}

		System.out
				.println("=================================3D Int Array Elements====================================");
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				for (int k = 0; k < a[i][j].length; k++) {
					System.out.print(a[i][j][k] + "  ");
				}
				System.out.println("");
			}
		}

		System.out.println(
				"=================================3D String Array Elements====================================");
		for (int i = 0; i < s.length; i++) {
			for (int j = 0; j < s[i].length; j++) {
				for (int k = 0; k < s[i][j].length; k++) {
					System.out.print(s[i][j][k] + "  ");
				}
				System.out.println("");
			}
		}
		input.close();
	}

}
