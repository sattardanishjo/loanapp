package com.abdul;

import java.sql.Connection;
import java.sql.DriverManager;

public class JdbcConection {

	public static Connection getDBConnection() throws Exception {
		Connection conn = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			// connection string
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/training", "root", "Afghan123");
			return conn;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
