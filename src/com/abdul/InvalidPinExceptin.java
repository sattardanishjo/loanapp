package com.abdul;

public class InvalidPinExceptin extends Exception {

	String customerDisplayMessage;
	int errorCode;

	public InvalidPinExceptin(String errorMess, int errorCode) {

		super(errorMess);
		customerDisplayMessage = errorMess;
		this.errorCode = errorCode;

	}
}
